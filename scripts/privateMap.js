

getData('https://frightful-tomb-54925.herokuapp.com/user_locations').then(data => {
    loadUsersLocations(data.users)});


function loadUsersLocations(users){

    if(users.length === 0) return;

    var mymap = L.map('map').setView([users[0].location.latitude, users[0].location.longitude], 13-1*users.length);
    
    getData('https://frightful-tomb-54925.herokuapp.com/access_token').then(data => {
        let accessToken = data.token;
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: accessToken
        }).addTo(mymap);
    }
    );
    
    
    for(let user of users){
        let marker = L.marker([user.location.latitude, user.location.longitude]).addTo(mymap);
        marker.bindPopup(`<b>${user.name}</b><br>${user.timestamp}`);
    }
}

async function getData(url = '') {
    const response = await fetch(url, {
        method: 'GET'
    });
    return response.json(); // parses JSON response into native JavaScript objects
}