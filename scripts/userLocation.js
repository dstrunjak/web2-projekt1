function locateUser() {
    let status = document.querySelector('#status');
    let location = document.querySelector('#location');
    let map = document.querySelector('#map');

    location.textContent = '';
    
    function success(position) {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
        status.textContent = '';
        location.textContent = `Current location: Latitude = ${latitude} °, Longitude = ${longitude} °`;

        map.innerHTML = `<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"  
        src="https://www.openstreetmap.org/export/embed.html?bbox=${longitude-0.01}%2C${latitude-0.01}%2C${longitude+0.01}%2C${latitude+0.01}&amp;layer=mapnik&amp;marker=${latitude}%2C${longitude}" 
        style="border: 1px solid black">
        </iframe>
        <br/>
        <small>
        <a href="https://www.openstreetmap.org/?mlat=${latitude}&amp;mlon=${longitude}#map=16/${latitude}/${longitude}">View Larger Map</a></small>`

        return latitude, longitude;
    }
    
    function error() {
        status.textContent = 'Unable to retrieve your location';
    }
    
    if(!navigator.geolocation) {
        status.textContent = 'Geolocation is not supported by your browser';
    } else {
        status.textContent = 'Locating…';
        navigator.geolocation.getCurrentPosition(success, error);
    }
    
}



locateUser()


